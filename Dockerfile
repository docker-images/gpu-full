ARG NVIDIA_TAG=
FROM nvcr.io/nvidia/${NVIDIA_TAG}

RUN apt update && apt install -y python3-pip git vim && apt-get clean
RUN pip3 install --upgrade pip
RUN pip3 install setuptools jupyterlab matplotlib

EXPOSE 8888

# Use non interactive to bypass tzdata post install
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y openssh-server curl && apt-get clean

# pre-create /run/sshd
RUN service ssh start
EXPOSE 22

# Install Code-Server
RUN curl -fsSL https://code-server.dev/install.sh | sh
EXPOSE 8080

RUN echo PATH=$PATH > /etc/environment

# Some TF tools expect a "python" binary
RUN ln -s $(which python3) /usr/local/bin/python

ENV TF_FORCE_GPU_ALLOW_GROWTH=true

RUN apt update && apt install -y sudo curl && apt-get clean

RUN curl -fsSL https://raw.githubusercontent.com/filebrowser/get/master/get.sh | bash
RUN /usr/local/bin/filebrowser config init --address 0.0.0.0 --port 8081 --database /etc/filebrowser/filebrowser.db --branding.name MyDocker
EXPOSE 8081

COPY ./streamlit /template/streamlit
EXPOSE 8082

RUN sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && \
	sh -c 'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -' && \
	apt-get update && \
	apt-get -y install postgresql-15 gosu && \
	apt-get clean autoclean
COPY --from=postgres:15 /usr/local/bin/docker-entrypoint.sh /usr/local/bin/postgres-docker-entrypoint.sh
RUN mkdir -p /docker-entrypoint-initdb.d
COPY ./init-postgres.sh /usr/local/bin
RUN chmod +x /usr/local/bin/init-postgres.sh
EXPOSE 5432

ENV JUPYTER=1
ENV SSH=1
ENV VSCODE=1
ENV FILEBROWSER=1
ENV POSTGRESQL=1
ENV STREAMLIT=1
ENV SLEEP=1

COPY docker-init.sh /usr/local/bin/docker-init.sh
RUN chmod +x /usr/local/bin/docker-init.sh

ENTRYPOINT ["/bin/bash", "/usr/local/bin/docker-init.sh"]
