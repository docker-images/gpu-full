# GPU Full

## Usage

```sh
docker run \
    -p 8022:22 \
    -p 8888:8888 \
    -p 8080:8080 \
    -p 8081:8081 \
    -p 8082:8082 \
    -v ~/project:/home \
    gitlab-research.centralesupelec.fr:4567/docker-images/gpu-full/gpu-full:11.7 \
    user \
    pass
```

### Ports

- 8022: SSH. Login with `ssh user@localhost -p 8022`
- 8888: JupyterLab. You must provide the password.
- 8080: VSCode. You must provide the password.
- 8081: FileBrowser. You must provide the login and the password
- 8082: Streamlit

You can also expose the PostgreSQL `5432` port, but beware to not expose it on internet.

### Volume

Change `~/project` by your workdir.

### Database

You can use the CLI inside the container (ssh, remote terminal in vscode, ...) with `psql`.
