#!/bin/bash

set -e

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters. Please use two arguments, username and password"
    exit 1
fi

echo "$(hostname -I | cut -f2 -d' ') $HOSTNAME" >> /etc/hosts

if id "ubuntu" >/dev/null 2>&1; then
    userdel ubuntu
fi

useradd --system --create-home --shell /bin/bash -u 1000 ${1}

cp --update=none /etc/skel/.* /home/${1}/

echo "$1":"$2" | chpasswd
usermod -aG sudo ${1}
chown "$1" /home/${1}/
chmod 755 /home/${1}/

if [ ! -f "/home/${1}/.config/pip/pip.conf" ]; then
    mkdir -p /home/${1}/.config/pip && echo -e "[global]\nbreak-system-packages = true" > /home/${1}/.config/pip/pip.conf
    chown -R "$1:$1" /home/${1}/.config/
fi

rm -rf /home/${1}/.cache
mkdir /tmp/cache
ln -s /tmp/cache /home/${1}/.cache
chown ${1}:${1} /tmp/cache /home/${1}/.cache

sigint() {
    echo "SIGINT received"
    if [ "${SSH}" -eq 1 ]; then
        pkill -INT sshd
    fi
    if [ "${JUPYTER}" -eq 1 ]; then
        pkill -INT jupyter-lab
    fi
    if [ "${VSCODE}" -eq 1 ]; then
        pkill -INT code-server
    fi
    if [ "${FILEBROWSER}" -eq 1 ]; then
        pkill -INT filebrowser
    fi
    if [ "${POSTGRESQL}" -eq 1 ]; then
        pkill -INT postgres
    fi
    if [ "${STREAMLIT}" -eq 1 ]; then
        pkill -INT streamlit
    fi
    if [ "${SLEEP}" -eq 1 ]; then
        pkill -INT sleep
    fi
    exit
}
trap sigint INT

sigterm() {
    echo "SIGTERM received"
    if [ "${SSH}" -eq 1 ]; then
        pkill -TERM sshd
    fi
    if [ "${JUPYTER}" -eq 1 ]; then
        pkill -TERM jupyter-lab
    fi
    if [ "${VSCODE}" -eq 1 ]; then
        pkill -TERM code-server
    fi
    if [ "${FILEBROWSER}" -eq 1 ]; then
        pkill -TERM filebrowser
    fi
    if [ "${SLEEP}" -eq 1 ]; then
        pkill -TERM sleep
    fi
    if [ "${POSTGRESQL}" -eq 1 ]; then
        pkill -TERM postgres
    fi
    if [ "${STREAMLIT}" -eq 1 ]; then
        pkill -TERM streamlit
    fi
    exit
}
trap sigterm TERM

export WORKDIR=/home/${1}/workdir

mkdir -p ${WORKDIR} && chown ${1}:${1} ${WORKDIR}

if [ "${SSH}" -eq 1 ]; then
    echo "Start sshd"
    /usr/sbin/sshd
    status=$?
    if [ $status -ne 0 ]; then
        echo "Failed to start sshd: $status"
        exit $status
    fi
    echo "sshd started"
fi

if [ "${JUPYTER}" -eq 1 ]; then
    echo "Start Jupyter"

    su - ${1} -c "JUPYTER_TOKEN=${2} jupyter-lab \
        --MappingKernelManager.cull_idle_timeout=3600 \
        --MappingKernelManager.cull_interval=300 \
        --TerminalManager.cull_inactive_timeout=3600 \
        --TerminalManager.cull_interval=300 \
        --allow-root \
        --ip=0.0.0.0 \
        --notebook-dir=${WORKDIR} \
        &"
fi

if [ "${VSCODE}" -eq 1 ]; then
    echo "Start code-server"
    mkdir -p /home/${1}/.config/code-server && chown ${1}:${1} /home/${1}/.config/code-server
    echo "bind-addr: 0.0.0.0:8080
auth: password
password: ${2}
cert: false
disable-telemetry: true
disable-update-check: true
user-data-dir: /home/${1}/.vscode-data
extensions-dir: /home/${1}/.vscode-extensions
    " > /home/${1}/.config/code-server/config.yaml

    su - ${1} -c "/usr/bin/code-server ${WORKDIR} &"
fi

if [ "${FILEBROWSER}" -eq 1 ]; then
    /usr/local/bin/filebrowser --database /etc/filebrowser/filebrowser.db users add "${1}" "${2}"
    echo "Start filebrowser"
    /usr/local/bin/filebrowser --database /etc/filebrowser/filebrowser.db --root /home/${1} &
    status=$?
    if [ $status -ne 0 ]; then
        echo "Failed to start filebrowser: $status"
        exit $status
    fi
    echo "filebrowser started"
fi

if [ "${POSTGRESQL}" -eq 1 ]; then
    echo "Start postgresql"
    export PATH="${PATH}:/usr/lib/postgresql/15/bin"
    export PGDATA="/home/${1}/.pgdata"
    export POSTGRES_USER="${1}"
    export POSTGRES_PASSWORD="${2}"
    /usr/local/bin/init-postgres.sh
    su - postgres -c "/usr/lib/postgresql/15/bin/pg_ctl -D ${PGDATA} -o \"-c listen_addresses='*'\" start"
    echo "postgres started"
fi

if [ "${STREAMLIT}" -eq 1 ]; then
    echo "Start streamlit"
    cp -rn /template/streamlit /home/${1}/workdir
    
    # Set +e because Streamlite create temporary file, and  chown may fail on it (not)
    set +e
    chown -R ${1}:${1} /home/${1}/workdir/streamlit
    set -e
    if [ -f /home/${1}/workdir/streamlit/requirements.txt ]; then
        su - ${1} -c "pip install -r /home/${1}/workdir/streamlit/requirements.txt"
    fi
    if [ "${POSTGRESQL}" -eq 1 ]; then
        STREAMLIT_SECRETS="/home/${1}/workdir/streamlit/.streamlit/secrets.toml"
        if [ -f "${STREAMLIT_SECRETS}" ]; then
            sed -ri "/\[connections.postgresql\]/,/\[/ s/(.*username.* = .*)/username = \"${1}\"/" "${STREAMLIT_SECRETS}"
            sed -ri "/\[connections.postgresql\]/,/\[/ s/(.*password.* = .*)/password = \"${2}\"/" "${STREAMLIT_SECRETS}"
        fi
    fi
    su - ${1} -c "cd /home/${1}/workdir/streamlit/ && streamlit run --server.port 8082 app.py &"
    echo "streamlit started"
fi

if [ "${SLEEP}" -eq 1 ]; then
    while :; do
        sleep 1s
    done
fi
